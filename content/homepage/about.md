---
title: 'Ne yapıyoruz?'
weight: 1
background: ''
button: 'Daha fazla'
buttonLink: 'about'
---

Gayrimenkul yatırım danışmanlık firmamız, müşterilerimize gayrimenkul yatırımı konusunda uzman tavsiyeler sunarak, en doğru yatırım kararlarını almalarına yardımcı oluyoruz. Yatırım amaçlı gayrimenkul seçimi, portföy yönetimi, kira ve satış işlemleri, yatırım getirisi analizi ve raporlama gibi konuları kapsayan geniş bir hizmet yelpazesi sunuyoruz. Müşterilerimizin memnuniyeti bizim için en önemli önceliktir ve bu doğrultuda hizmet veriyoruz.
